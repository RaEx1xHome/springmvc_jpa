/**
 * 
 */
package springmvc_jpa.com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import springmvc_jpa.com.dao.UserInfoDao;
import springmvc_jpa.com.entity.UserInfo;
import springmvc_jpa.com.service.UserInfoService;

/**
 * @author Administrator
 *
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {


    @Autowired
    private UserInfoDao userInfoDao;

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public <T> void insert(T entity) {
		userInfoDao.insert(entity);
		
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public <T> void update(T entity) {
		userInfoDao.update(entity);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public <T> void delete(T entity) {
		userInfoDao.delete(entity);
		
	}

	@Override
	public <T> void refresh(T entity) {
		userInfoDao.refresh(entity);
	}
	
	@Override
	public <T> List<T> findAll(Class<T> clazz) {
		return userInfoDao.findAll(clazz);
	}

	@Override
	public <T> T findById(Class<T> clazz, Object id) {
		return userInfoDao.findById(clazz, id);
	}

}
