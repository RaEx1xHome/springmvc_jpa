/**
 * 
 */
package springmvc_jpa.com.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import springmvc_jpa.com.dao.BaseDao;

/**
 * @author Administrator
 *
 */
@Repository
public class BaseDaoImpl implements BaseDao {
	// 注入实体管理器
	@PersistenceContext
	protected EntityManager em;
	
	@Override
	public <T> void insert(T entity) {
		em.persist(entity);
		em.flush();
	}

	@Override
	public <T> void update(T entity) {
		if (em.contains(entity)) {
			em.merge(entity);
		}else {
			em.persist(entity);
		}
		em.flush();
	}

//	@Override
//	public void merge(Object entity) {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public <T> void delete(T entity) {
		// TODO Auto-generated method stub
		em.remove(entity);
	}


	@Override
	public <T> void refresh(T entity) {
		// TODO Auto-generated method stub
		em.refresh(entity);
	}
	
	@Override
	public <T> T findById(Class<T> clazz, Object id) {
		// TODO Auto-generated method stub
		return em.find(clazz, id);
	}


	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findAll(Class<T> clazz) {
		// TODO Auto-generated method stub
		String qlString = "select t from UserInfo t";
		Query query = em.createQuery(qlString);
		List<T> list = query.getResultList();
		return list;
	}

}
