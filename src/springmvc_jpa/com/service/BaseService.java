/**
 * 
 */
package springmvc_jpa.com.service;

import java.util.List;

import springmvc_jpa.com.entity.UserInfo;

/**
 * @author Administrator
 *
 */
public interface BaseService {

    /**
     * 保存实体
     * @param entity
     */
    public <T> void insert(T entity);
    
    /**
     * 更新实体
     * @param entity
     */	
    public <T> void update(T entity);
	
//    /**
//     * 合并实体
//     * @param entity
//     */
//	public void merge(Object entity);
	
    /**
     * 删除实体
     * @param entity
     */
	public <T> void delete(T entity);
	
    /**
     * 同步实体数据
     * @param entity
     */
	public <T> void refresh(T entity);
	
    /**
     * 根据主键查询实体
     * @param <T>
     * @param clazz  实体类
     * @param id     主键
     * @return
     */
	public <T> T findById(Class<T> clazz,Object id);

    /**
     * 获取实体所有对象
     * @param <T>
     * @param <T>
     * @param clazz  实体类
     * @return
     */
	public <T> List<T> findAll(Class<T> clazz);

	
	

    
}
